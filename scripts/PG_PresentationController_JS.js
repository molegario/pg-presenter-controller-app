// Scope all code so that we can control what seeps into the window object
(function () {
    var PG_PresentationController_JS = function() {
    };

    /**
     * X2O Object properties are typed, and a few components of the system are sensitive to this type (Channel Designer's property list dialog
     * will display a specific input field for certain types, for example). Values not specified in this list will be tolerated, but have no
     * special effect in the system.
     *
     * Each value is defined here with its name followed by the input control shown by Channel Designer and notes if applicable, separated by "//";
     * names are case-insensitive.
     *
     * So although this documentation presents a data structure, this is in fact a list of all possible values (that also explains why all "properties"
     * below are presented with a type of {string}, regardless of the information they carry) for an X2O object's custom property type.
     *
     * @property {string} Audio         The identifier of an audio asset.   <br>A dialog that navigates the current network, restricted to audio assets.
     * @property {string} Bool          A value of "True" or "False".       <br> A drop-down with solely "True" or "False" as options.
     * @property {string} Boolean       A value of "True" or "False".       <br> A drop-down with solely "True" or "False" as options.
     * @property {string} Channel       A channel in the X2O Platform.      <br> A channel selector dialog to locate and select a channel.
     * @property {string} Color         Represents a color value (RGB).     <br> A color-picker, where the user can also set an RGB value manually.
     * @property {string} Colorlist     Represents a color value (RBG).     <br> A list of pre-defined colors to pick from doubled with a manual entry textbox.
     * @property {string} Contentfolder Path to a content folder.           <br> A treeview-based content folder browser/selector.
     * @property {string} Customeditor  Specialized text content.           <br> A dialog containing an editor as defined by the object. (See x2o.ObjectBase API documentation)
     * @property {string} DataDocument  The identifier of a known data document. <br> A dialog that allows navigating known data documents.
     * @property {string} Datafeed      The identifier of a known datafeed. <br> A dialog that allows navigating known datafeeds.
     * @property {string} Excel         The identifier of an Excel asset.   <br> A dialog that navigates the current network, restricted to Excel assets.
     * @property {string} File          The identifier of a file asset.     <br> A dialog that navigates the current network, restricted to file assets.
     * @property {string} Flash         The identifier of a flash asset.    <br> A dialog that navigates the current network, restricted to Flash assets.
     * @property {string} Flipbook      The identifier of a flipbook asset. <br> A dialog that navigates the current network, restricted to flipbook assets.
     * @property {string} Float         A floating point value.             <br> A textbox restricted to floating point number entry.
     * @property {string} Format        A temperature format indicator.     <br> A dropdown allowing for one of &deg;C or &deg;F or player setting.
     * @property {string} Font          A JSON-encoded font definition      <br> Although this field can be manually edited, it interacts with the font
     *                                                                           toolbar in Channel Designer; the object properties dialog only provides a
     *                                                                           raw text editor for this data.
     * @property {string} Image         The identifier of an image asset.   <br> A dialog that navigates the current network, restricted to image assets.
     * @property {string} Integer       An integer value.                   <br> A textbox restricted to numeric entry.
     * @property {string} Language      A language code identifier.         <br> A drop-down allowing selection from a few pre-defined languages, or player setting.
     * @property {string} Misc          The identifier of a misc asset.     <br> A dialog that navigates the current network, restricted to misc assets.
     * @property {string} Objectname    A valid object name.                <br> A textbox which respects Channel Designer object name restrictions.
     *
     * @property {string} Option{[values]} One of a set of pre-defined values.<br>
     *            A drop-down out of a possible values. <p>
     *            Values are to be supplied directly in the property typename, using hash keys as returned values and hash values as
     *            display text; {"1": "Today", "2": "Tomorrow"} would yield a drop-down with "Today" and "Tomorrow" as display entries
     *            and "1" and "2" as corresponding values. **It is highly recommended to supply this value as `'option' +
     *            JSON.stringify(object)`** as it guarantees proper JSON-encoding of the values.
     *
     * @property {string} Password      A string but displayed with asterisks.  <br> A textbox for freeform entry, that doesn't show the text.
     * @property {string} PDF           The identifier of a pdf asset.       <br> A dialog that navigates the current network, restricted to PDF assets.
     * @property {string} Playlist      An identifier to a playlist.         <br> A dialog tha navigates playlists for the current network.
     * @property {string} Powerpoint    The identifier of a PowerPoint asset.<br> A dialog that navigates the current network, restricted to PowerPoint assets.
     * @property {string} Richtext      Text value meant for rich content.<br> A dialog that offers an editor for rich text.
     * @property {string} Stretchmode   One of the possible stretchmodes.    <br> A drop-down with pre-defined values: keepaspect, stretch, originalsize.
     * @property {string} String        A string literal.                    <br> A textbox for freeform entry.
     * @property {string} Twitter       Possible Twitter operating modes.    <br> A drop-down with search modes by hash tag, by user account or freeform query.
     * @property {string} Video         The identifier of a video asset.     <br> A dialog that navigates the current network, restricted to video assets.
     * @property {string} Wayfinder     The identifier of a wayfinder asset. <br> A dialog that navigates the current network, restricted to wayfinder assets.
     */
    var objectProperties = [
        {
            name: "UserName",
            type: "string",
            description: "Enter the user name",
            extendedDescription: "User name is used to display the message \"Hello {{UserName}}\"",
            value: 'World',
            mainProperty: true
        },
        {
            name: "Color",
            type: "Color",
            description: "Font color",
            extendedDescription: "Color of the displayed text",
            value: '#ffffff'
        }
    ];

    // Inheritance setup
    PG_PresentationController_JS.inheritsFrom(x2o.ObjectBase);

    /**
     * This method is called when internal property values of the object have changed, as a result of:
     *  - Channel Designer's property dialog being used to modify properties at design-time
     *  - An object being brought to life at run-time
     *  - A data-bound property being updated automagically
     *  - Anytime an external entity calls `public.setProperties` on the object (see {@link x2o.ObjectBase~DefaultPublicInterface})
     *
     * @see x2o.ObjectBase#onPropertiesUpdated
     *
     * @param {string[]} updatedProps   A list containing the names of all properties affected by the operation
     * @param {string[]} updatedData    A list containing the names of all data-bound properties affected by the operation
     */
    PG_PresentationController_JS.prototype.onPropertiesUpdated = function (updatedProps, updated_data) {
        if(updatedProps.indexOf('UserName') > -1) {
            this.setUserName(this.properties_.values['UserName']);
        }

        if(updatedProps.indexOf('Color') > -1) {
            this.setColor(this.properties_.values['Color']);
        }
    };

    /**
     * Set text color for the message.
     *
     * @param color The new color value
     */
    PG_PresentationController_JS.prototype.setColor = function(color) {
        $('.message').css({
            color: color
        });
    };

    /**
     * Update the user name displayed
     *
     * @param name  The name to display
     */
    PG_PresentationController_JS.prototype.setUserName = function(name) {
        // Update displayed name
        $('#nameProperty').text(name);
    };

    /**
     * Object initialisation
     */
    PG_PresentationController_JS.prototype.init = function () {
        var self = this;
        var deferred = $.Deferred();
    
        /**
         * See Objects API documentation (Platform 
         */
        self.setInitOptions({
            showHeader: true,           // Default value
            title: {
                value: 'Title'          // Default value
            },
            backgroundStyle: 'object'   // Default value
        });
        
        $.when(this.construct(
            window.name,
            objectProperties
        )).then(function () {
            // Raises an event to signify that this object is ready to be used.
            // Only call this function once the object has finished initializing for 
            // the first time.
            // 
            // A channel will keep track of all existing object
            // and fire 'Channel:AllObjectsReady' when all objects have triggered this event.
            self.notifyObjectReady({
                name: PG_PresentationController_JS
            });

            deferred.resolve();
        });

        return deferred.promise();
    };

    // Export object from namespace
    window.PG_PresentationController_JS = new PG_PresentationController_JS();

    //Bootstrap
    $(function () {
        $.when(window.PG_PresentationController_JS.init()).then(function () {
            // POST-CONSTRUCTION CODE
            var app = $.sammy('#main', function () {
                //X2O vars
                var client = false,
                cartridgeId = 'f5ffdd31-7e9c-49c8-8934-26b4dbb749a6',
                epoxy_events_handler = {
                    connected: function () {
                        console.log("Epoxy client connected - client handler!");
                    },
                    disconnected: function() {
                        console.log("Epoxy client disconnected - client handler!");
                        client.unregisterClient(cartridgeId, epoxy_events_handler);
                    },
                    incoming: function (msg) {
                        console.log("Epoxy client data push received - client handler!");
                        app.trigger('x2o.epoxy.push', msg);
                    }
                },
                volume_increment = 0.05;

                //STOPWATCH INSTANCE
                var seconds = 0, minutes = 0, hours = 0,
                t;

                function add() {
                    seconds++;
                    if (seconds >= 60) {
                        seconds = 0;
                        minutes++;
                        if (minutes >= 60) {
                            minutes = 0;
                            hours++;
                        }
                    }
                    var textContent = (hours ? (hours > 9 ? hours : "0" + hours) : "00") 
                        + ":" + (minutes ? (minutes > 9 ? minutes : "0" + minutes) : "00") 
                        + ":" + (seconds > 9 ? seconds : "0" + seconds);
                    $('#pg-time-display-ct').text(textContent);
                    timer();
                }
    
                function timer() {
                    t = setTimeout(add, 1000);
                }

                //USE - Sammy Templates
                this.use('Template');
                this.use('Storage');

                //SESSION STATE
                var pgcookie = new Sammy.Store({
                    name: 'pgpresentercontroller',
                    type: 'cookie'
                });

                //INSTANCE STATE
                var pgstate = new Sammy.Store({
                    name: 'pgpresentercontroller',
                    type: 'memory'
                });

                //DEFAULT AUDIO SETTINGS
                var default_audio_settings = {
                    muted: false,
                    volume: .2
                };

                //Check logged in
                function checkLoggedIn (callback) {
                    //persist login from cookie
                    if(!pgstate.exists('current_user') && pgcookie.exists('current_user')) {
                        pgstate.set('current_user', pgcookie.get('current_user'));
                    }
                    //toggle state of logout button
                    if (pgstate.exists('current_user')) {
                        $('#app-logout').removeClass('d-none');
                    } else {
                        $('#app-logout').addClass('d-none');
                    }
                    callback();
                }

                //implement around of checkloggedin at every request
                this.around(checkLoggedIn);

                function sendAVCommand (avc_to_send) {
                    if(avc_to_send) {
                        client.sendCommand({
                            'Id':cartridgeId
                          },{
                            "Event": "RUN_AVCOMMAND",
                            "AVCommand": avc_to_send 
                          }).then(function(sessions){
                        });    
                    }
                }

                var throttled_avc = _.throttle(sendAVCommand, 3000);

                //UTILITY FUNCTIONS
                function getRecursiveAreasFromLayout (layout) {
                    return _.reduce(layout, function (acc, itm, idx) {
                        if ((itm.Children && (itm.Children.length > 0)) && (itm.Type !== 'Area')) {
                            acc.push(getRecursiveAreasFromLayout(itm.Children));
                        } else {
                            acc.push(itm);
                        }
                        return acc;
                    }, []);
                }

                function listAllAreasFromLayout (layout) {
                    return _.flatten(getRecursiveAreasFromLayout(layout));
                }

                function injectAreaDetails (area) {
                    var out = _.findWhere(pgstate.get('available_areas'), {
                        Id: area.Id
                    });
                    return _.extend({}, out, area);
                }

                function orderAreaList (areas) {
                    return _.compact(_.map(pgstate.get('available_areas'), function (itm, idx) {
                        return _.findWhere(areas, {
                            Id: itm.Id
                        });
                    }));
                }

                function isValidArea (area) {
                    return _.contains(_.pluck(pgstate.get('available_areas'), 'Id'), area.Id);
                }

                //init volume bar
                function initVolumeBar() {
                    //check session audio setting
                    if(pgcookie.exists('audio') && _.isNumber(pgcookie.get('audio'))) {
                        pgstate.set('audio', pgcookie.get('audio'));
                    } else {
                        pgstate.set('audio', 0);
                        pgcookie.set('audio', 0);
                    }

                    //VOLUME DOWN
                    $('#lo-button').click(function (evt) {
                        var volume = Math.max(Math.min(pgstate.get('audio') - 1, 2), 0);
                        pgstate.set('audio', volume);
                        pgcookie.set('audio', volume);
                        //populate AV array
                        var current_area_layout = _.findWhere(pgstate.get('valid_selected_areas'), {
                            Id: pgstate.get('current_area').Id
                        });
                        var current_area_screens =  _.map(current_area_layout.Children, function (itm, idx) {
                            return _.findWhere(pgstate.get('all_screens'), {
                                Id: itm.Id
                            })
                        });                      
                        var available_av_commands = _.flatten(_.pluck(current_area_screens, 'AVCommands'));
                        var setting_keys = ['Volume - Mute', 'Volume - Medium', 'Volume - Full'];
                        var avc_to_send = _.findWhere(available_av_commands, {
                            Name: setting_keys[pgstate.get('audio')]
                        });
                        if(avc_to_send) {
                           throttled_avc(avc_to_send);   
                        }
                    });
                    
                    //VOLUME UP
                    $('#hi-button').click(function (evt) {
                        var volume = Math.max(Math.min(pgstate.get('audio') + 1, 2), 0);
                        pgstate.set('audio', volume);
                        pgcookie.set('audio', volume);
                        //populate AV array
                        var current_area_layout = _.findWhere(pgstate.get('valid_selected_areas'), {
                            Id: pgstate.get('current_area').Id
                        });
                        var current_area_screens =  _.map(current_area_layout.Children, function (itm, idx) {
                            return _.findWhere(pgstate.get('all_screens'), {
                                Id: itm.Id
                            })
                        });                      
                        var available_av_commands = _.flatten(_.pluck(current_area_screens, 'AVCommands'));
                        var setting_keys = ['Volume - Mute', 'Volume - Medium', 'Volume - Full'];
                        var avc_to_send = _.findWhere(available_av_commands, {
                            Name: setting_keys[pgstate.get('audio')]
                        });

                        if(avc_to_send) {
                            throttled_avc(avc_to_send);
                        }
                    });
                }

                //ROUTE: #/, WEBROOT
                this.get('#/', function (context) {
                    if(pgstate.exists('current_user')) {
                        app.trigger('log.in');
                    }
                    context.partial('templates/login.template', {
                        item: {
                            message: 'Click \'Enter\' to proceed.'
                        }
                    });
                });

                function initStopwatch () {
                    /* Start/Stop button */
                    clearTimeout(t);
                    var watchstopped = false;
                    seconds = 0;
                    minutes = 0;
                    hours = 0;
                    timer();
                    $('#btnWatchStart').click(function (itm) {
                        watchstopped = !watchstopped;
                        $(itm.target).closest('button').removeClass('pg-stopped');
                        if(watchstopped) {
                            $(itm.target).closest('button').addClass('pg-stopped');
                            clearTimeout(t);
                        } else {
                            timer();
                        }
                    });

                    $('#btnWatchClear').click(function (evt) {
                        $('#pg-time-display-ct').text("00:00:00");
                        seconds = 0; minutes = 0; hours = 0;
                    });

                }

                function scrollToDisplayed (reverse) {
                    //scroll into view
                    var last_displayed = ! reverse ? _.last($('.is-displaying')) : _.first($('.is-displaying'));
                    if(last_displayed) {
                        last_displayed.scrollIntoView({
                            behavior: 'smooth'
                        });    
                    }
                }

                function initGridLayoutToggle (layout_class) {
                    //Listen to grid layout select update
                    $('.inner-slide-ct').addClass(layout_class);

                    $('.display-grid-icon-ct').click(function (evt) {
                        $('#pg-display-mode-ct').removeClass('layout-mode-selected');
                        $('.inner-slide-ct').removeClass('pg-focused-slides-display');    
                        _.delay(scrollToDisplayed, 200);
                    });

                    $('.display-layout-icon-ct').click(function (evt) {
                        $('#pg-display-mode-ct').addClass('layout-mode-selected');
                        $('.inner-slide-ct').addClass('pg-focused-slides-display');
                        _.delay(scrollToDisplayed, 200);
                    });
                }

                function setSliderCtHeight () {
                    var main_height = $('#main').outerHeight(true),
                        subnav_height = $('#pg-subnav-ct').outerHeight(true),
                        modetabs_height = $('#pg-mode-tabs').outerHeight(true),
                        displaymode_height = $('#pg-display-mode-ct').outerHeight(true),
                        footer_height = $('footer').outerHeight(true);
                    $('.slide-ct').outerHeight(main_height - subnav_height - modetabs_height - displaymode_height - footer_height);
                }

                function initNotesToggle () {
                    //listen to notes toggle
                    $('#btnNotes').click(function () {
                        if($('#speaker-notes-container').hasClass('d-none')) {
                            $('#speaker-notes-container').removeClass('d-none');
                            $('#btnNotes').text('Hide Notes');
                        } else {
                            $('#speaker-notes-container').addClass('d-none');
                            $('#btnNotes').text('Show Notes');
                        }
                        setSliderCtHeight();
                    });                    
                }

                function initLeaderToggle () {
                    $('input#isleader').change(function (evt) {
                        var leaderstate = $(evt.target).parent().find('input:checked').length > 0;
                        if(!leaderstate) {
                            $('#handoff-session').unbind('click');
                            $('#handoff-session').click(function (evt) {
                                app.trigger('leader.surrendered', {
                                    destination: '#/showtime/' + pgstate.get('current_session').Id
                                });
                                $('#handoffModal').modal('hide');
                            });
                            $('#handoffModalLabel').text('Handoff session leader');
                            $('#handoff-message').text('Click \'Proceed\' to hand-off this session to another leader.');
                            $('#handoffModal').modal('show');
                        } else {
                            $('#handoff-session').unbind('click');
                            $('#handoff-session').click(function (evt) {
                                app.trigger('leader.obtain', {
                                    destination: '#/showtime/' + pgstate.get('current_session').Id
                                });
                                $('#handoffModal').modal('hide');
                            });
                            $('#handoffModalLabel').text('Take session leader');
                            $('#handoff-message').text('Click \'Proceed\' to take leadership of this session.');
                            $('#handoffModal').modal('show');
                        }
                    });
                    $('#handoff-cancel').click(function (evt) {
                        $('#handoffModal').modal('hide');
                        _.defer(function () {
                            $('input#isleader').click();
                        });
                    });
                }

                function initEditToggle () {
                    $('input#iseditable').change(function (evt) {
                        var editablestate = $(evt.target).parent().find('input:checked').length > 0;
                        if(editablestate) {
                            $('#sortable').sortable('enable');
                            $('.pg-editable .leader-btn-label').text('Unlocked')
                        } else {
                            $('#sortable').sortable('disable');
                            $('.pg-editable .leader-btn-label').text('Locked')
                        }
                    });                    
                }

                function getNextArea(area) {
                    var position = _.findIndex(pgstate.get('valid_selected_areas'), function (itm, idx) {
                        return itm.Id === area.Id;
                    });
                    return position > -1 ? (pgstate.get('valid_selected_areas')[position + 1] || false) : false;
                }

                function getPrevArea(area) {
                    var position = _.findIndex(pgstate.get('valid_selected_areas'), function (itm, idx) {
                        return itm.Id === area.Id;
                    });
                    return position > -1 ? (pgstate.get('valid_selected_areas')[position - 1] || false) : false;
                }

                //ROUTE: #/sessions, Sessions List
                this.get('#/sessions', function (context) {
                    if(!pgstate.exists('current_user')) {
                        this.redirect('#/');
                    }

                    x2o.api.epoxy.get().then(function(epoxy){
                        client = epoxy;
                        client.connect(undefined, 'controller').then(function () {

                            //if session unregister
                            if(pgstate.exists('current_session')) {
                                client.sendCommand({
                                    "Id": cartridgeId
                                },{
                                    "Event": "UNREGISTER"
                                }).then(function (response) {
                                    console.log('UNREGISTERSUCCESS');
                                });    
                            }

                            client.registerClient(cartridgeId, epoxy_events_handler);
                            client.sendCommand({
                                "Id": cartridgeId
                            },{
                                "Event": "REGISTER",
                                "Data": {
                                    "Type": "Controller",
                                    "NetworkId": getNetworkID()
                                }
                            }).then(function (response) {
                                if(response.success) {
                                    client.sendCommand({
                                        "Id": cartridgeId
                                    }, {
                                        "Event": "GET_ACTIVE_SESSIONS"
                                    }).then(function (response) {
                                        if(response.success) {
                                            var all_slides = _.pluck(_.flatten(_.pluck(_.flatten(_.pluck(response.data, 'Areas')), 'Slides')), 'Thumbnail');
                                            var all_refs = _.map(all_slides, function (itm, idx) {
                                                var regex = /https?:\/\/\S+\/XmanagerWeb\/Xynco\/flipbooks\/(\S+)\//;
                                                var m = regex.exec(itm);
                                                return m ? m[1] : '';
                                            });
                                            var unique_assets = _.compact(_.uniq(all_refs));

                                            poll_flipbooks = _.map(unique_assets, function (itm, idx) {
                                                return $.get("/XManagerWeb/Xynco/handlers/flipbook.ashx?method=info&id=" + itm);
                                            });

                                            $.when(poll_flipbooks).then(function () {
                                                context.partial('templates/sessions.template', {
                                                    items: response.data
                                                }, function (context) {
                                                    //set scroll monitor state
                                                    pgstate.set('sessions_isScrolling', false);

                                                    //set list height
                                                    var main_height = $('#main').outerHeight(true),
                                                        header_height = $('#sessions-header').outerHeight(true);
                                                    $('.sessions-list-ct').outerHeight(main_height - header_height - 30);
                                                    
                                                    //fancy scroll
                                                    $('.sessions-list-ct').mCustomScrollbar({
                                                        theme: '3d-thick-dark',
                                                        snapAmount: 40,
                                                        scrollInertia: 25,
                                                        autoHideScrollbar: false,
                                                        scrollButtons: {
                                                            enable: true
                                                        },
                                                        callbacks: {
                                                            onScrollStart: function () {
                                                                pgstate.set('sessions_isScrolling', true);
                                                            },
                                                            onScroll: function () {
                                                                pgstate.set('sessions_isScrolling', false);
                                                            }
                                                        }
                                                    });
                                                    
                                                    //on session click
                                                    $('.btn-session').click(function (evt) {
                                                        if(pgstate.get('sessions_isScrolling')) {
                                                            return;
                                                        }
                                                        var sessionid = $(evt.target).data('sessionId');
                                                        var item_label = $(evt.target).text();
                                                        $('#joinModalMsg').text('Join session ' + item_label + '?');
                                                        $('#session-join').data('sessionId', sessionid);
                                                        $('#selectModal').modal('show');
                                                    });

                                                    //dialog join confirm
                                                    $('#session-join').click(function (evt) {
                                                        var sessionid = $(evt.target).data('sessionId');
                                                        app.trigger('join.session', {
                                                            sessionid: sessionid
                                                        });
                                                        $('#selectModal').modal('hide');
                                                    });
                                                });
                                            });
                                        }
                                    });
                                } else {
                                    context.partial('templates/error.template', {
                                        item: {
                                            message: 'Registration Failed.'
                                        }
                                    });
                                }
                            });
                            context.log("Epoxy client connected!");
                        })
                        .fail(function (err) {
                            context.log("Connection to server failed: " + err);
                            context.partial('templates/login.template', {
                                item: {
                                    message: "Connection to server failed: " + err
                                }
                            });
                        });
                    });
                });

                //POST: #/login, Login POST
                this.post('#/login', function (context) {
                    var current_user = _.extend({
                        Username: 'TESTUSER'
                    }, context.params);
                    pgstate.set('current_user', current_user);
                    pgcookie.set('current_user', current_user);                    
                    app.trigger('log.in');
                    return false;
                })

                //GET: #/logout, Logout GET
                this.get('#/logout', function (context) {
                    app.trigger('log.out');
                    return false;
                });

                //DESIGN VIEW
                function pgInitDesign (context) {
                    var displayed = [],
                        // area_screen_count = 5,
                        layout_class = 'cards-x-';

                    var current_area_layout = _.findWhere(pgstate.get('valid_selected_areas'), {
                        Id: pgstate.get('current_area').Id
                    });

                    //determine layout style based on current area screens
                    var area_screen_count = current_area_layout ? current_area_layout.Children.length : 5;
                    layout_class += area_screen_count;

                    //slide inventory - reference
                    var slides_inventory = pgstate.get('current_area').Slides;
                    //init grid
                    $('#sortable').sortable({
                        forcePlaceholderSize: true,
                        forceHelperSize: true,
                        update: function (evt, ui) {
                            var list = _.map($('#sortable li h4.slide-number'), function (itm) {
                                return $(itm).data('positionIndex');
                            });
                            app.trigger('list.sorted', {
                                list: list
                            });
                            
                            var displayed_order = _.map($('.slide-ct h4'), function (itm, idx) {
                                return parseInt($(itm).data('positionIndex'));
                            });

                            var slides_ordered = _.map(displayed_order, function (itm, idx) {
                                return slides_inventory[itm];
                            });

                            var updated_area = _.extend({}, pgstate.get('current_area'), {
                                Slides: slides_ordered,
                                CurrentIndex: -1
                            });

                            var updated_areas = _.map(pgstate.get('valid_selected_areas'), function (itm, idx) {
                                return itm.Id === updated_area.Id ? updated_area : itm;
                            })

                            var updated_session = _.extend({}, pgstate.get('current_session'), {
                                Areas: updated_areas
                            });

                            client.sendCommand({
                                "Id": cartridgeId
                            }, {
                                "Event": "MODIFY_SESSION",
                                "Session": updated_session
                            }).then(function (response) {
                                if(response.success) {
                                    console.log('SESSION UPDATED SUCCESS');
                                } else {
                                    console.log('SESSION UPDATED FAILED');
                                }
                            });

                            pgstate.set('current_area', updated_area);
                            pgstate.set('current_session', updated_session);
                        }
                    });

                    //set card inner text not selectable
                    $('#sortable').disableSelection();
                    $('#sortable').sortable('disable');
                    pgstate.set('design_isScrolling', false);

                    setSliderCtHeight();

                    $('.slide-ct').mCustomScrollbar({
                        theme: '3d-thick-dark',
                        scrollInertia: 25,
                        autoHideScrollbar: false,
                        scrollButtons: {
                            enable: true
                        },
                        callbacks: {
                            onScrollStart: function () {
                                pgstate.set('design_isScrolling', true);
                            },
                            onScroll: function () {
                                pgstate.set('design_isScrolling', false);
                            }
                        }
                    });

                    $('.slide-ct .card').each(function (i, itm) {
                        $(itm).removeClass('is-selected');
                        $(itm).click(function (evt) {
                            //marked selected
                            $('.slide-ct .card').removeClass('is-selected');
                            $(evt.target).closest('.slide-ct .card').addClass('is-selected');

                            if(pgstate.get('design_isScrolling')) {
                                return;
                            }

                            //mark grouping highlight
                            var listindex = $(evt.target).closest('.slide-ct .card').index();

                            //find lead card of group clicked card is in
                            var leadslideindex = Math.floor(listindex / area_screen_count) * area_screen_count;
                            //find all cards of selected group
                            var selected = _.filter($('.slide-ct .card h4'), function (itm, idx) {
                                return idx >= leadslideindex && idx < (leadslideindex + area_screen_count);
                            });
                            //reset all cards selected indicator
                            $('.slide-ct .card').each(function (i, itm) {
                                $(itm).removeClass('is-grouped');
                            });
                            //set selected row selected indicators
                            _.each(selected, function (itm, idx, list) {
                                $(itm).closest('.slide-ct .card').addClass('is-grouped');
                            })

                            //get notes for selected slide
                            var selected_slide_data = slides_inventory[parseInt($(evt.target).closest('.slide-ct .card').find('h4').first().data('positionIndex'))];
                            var regex = /0,(.+),0/gm,
                                m = regex.exec(selected_slide_data.SpeakerNotes);
                            if(m) {
                                $('#speakerNotes').html('<p>' + m[1] + '</p>');
                            } else {
                                $('#speakerNotes').html('<p>' + selected_slide_data.SpeakerNotes + '</p>');
                            }
                        });
                    });

                    //listen to leader changes
                    initLeaderToggle();

                    //listen to notes toggle
                    initNotesToggle();
                    
                    //listen to Editable changes
                    initEditToggle();

                    //Listen to select area update
                    $('#areaselect .dropdown-menu button').click(function (evt) {
                        var areaid = $(evt.target).data('areaId');
                        pgstate.set('current_area', _.findWhere(pgstate.get('valid_selected_areas'), {
                            Id: areaid
                        }));
                        pgstate.set('areas_rest', _.filter(pgstate.get('valid_selected_areas'), function (itm, idx) {
                            return itm.Id !== areaid;
                        }));
                        renderDesignPage(context);
                    });

                    //Listen to grid layout select update
                    initGridLayoutToggle(layout_class);

                    //frame details editor
                    $('.pg-configure-frame-details').click(function (evt) {
                        var slideindex = parseInt($(evt.currentTarget).parent().find('h4').first().data('positionIndex'));
                        var targetSlide = slides_inventory[slideindex];

                        //populate notes
                        var regex = /0,(.+),0/gm,
                        m = regex.exec(targetSlide.SpeakerNotes);
                        $('#detailsEditorModal').modal('show');
                        if(m) {
                            $('#detailsEditorModal textarea').val(m[1]);
                        } else {
                            $('#editor-collapseOne textarea').val(targetSlide.SpeakerNotes);
                        }

                        //populate AV array
                        var current_area_layout = _.findWhere(pgstate.get('valid_selected_areas'), {
                            Id: pgstate.get('current_area').Id
                        });

                        var current_area_screens =  _.map(current_area_layout.Children, function (itm, idx) {
                            return _.findWhere(pgstate.get('all_screens'), {
                                Id: itm.Id
                            })
                        });                      

                        var available_av_commands = _.flatten(_.pluck(current_area_screens, 'AVCommands'));
                        var selected_av_commands = _.filter(targetSlide.AVCommands, function (itm, idx) {
                            return _.contains(_.pluck(available_av_commands, 'Name'), itm.Name);
                        });

                        var av_commands_list = _.reduce(available_av_commands, function (acc, itm, idx) {
                            var out = 
                                '<label class="pg-custom-checkbox custom-control custom-checkbox d-flex align-items-center justify-content-start">'
                                + '<input type="checkbox" class="custom-control-input" data-area-id="AV-' + idx + '"'
                                + (_.contains(_.pluck(selected_av_commands, 'Name'), itm.Name) ? ' checked' : '') + '>'
                                + '<span class="custom-control-indicator mr-4"></span>'
                                + '<span class="custom-control-description frutiger-light font-16">' + itm.Name + '</span>'
                                + '</label>';
                            return acc + out;
                        }, '');

                        //make text area editable on click
                        $('#editor-collapseOne textarea').click(function (evt) {
                            var current_value = $(evt.currentTarget).val();
                            $(evt.currentTarget).focus();
                            $(evt.currentTarget).val('');
                            $(evt.currentTarget).val(current_value);
                        });

                        //reset details editor display to notes
                        $('#editor-collapseOne').addClass('show');
                        $('#editor-collapseTwo').removeClass('show');
                        //push avc to list display panel
                        $('#editor-collapseTwo .card-body').html(av_commands_list);
                        //set save method to modal save button
                        
                        $('#frame-editor-save').unbind('click').click(function (evt) {
                            //compile changes to session
                            var updated_notes = $('#editor-collapseOne textarea').val();
                            var updated_AVCommands = _.map($('#editor-collapseTwo input:checked'), function (itm, idx) {
                                var command_name = $(itm).next().next().text();
                                var out = _.findWhere(available_av_commands, {
                                    Name: command_name
                                });
                                return out;
                            });

                            var updated = _.extend({}, targetSlide, {
                                SpeakerNotes: updated_notes,
                                AVCommands: updated_AVCommands
                            });

                            var updated_slides = _.map($('.slide-ct .card h4'), function (itm, idx) {
                                if(parseInt($(itm).data('positionIndex')) === slideindex) {
                                    slides_inventory[parseInt($(itm).data('positionIndex'))] = updated;
                                    return updated;
                                } else {
                                    return slides_inventory[parseInt($(itm).data('positionIndex'))];
                                }
                            });

                            var updated_areas = _.map(pgstate.get('valid_selected_areas'), function (itm, idx) {
                                if(itm.Id === pgstate.get('current_area').Id) {
                                    return _.extend({}, itm, {
                                        Slides: updated_slides
                                    });
                                } else {
                                    return itm;
                                }
                            });

                            pgstate.set('current_area', _.findWhere(updated_areas, {
                                Id: pgstate.get('current_area').Id
                            }));

                            var updated_session = _.extend({}, pgstate.get('current_session'), {
                                Areas: updated_areas
                            });

                            pgstate.set('current_session', updated_session);
                            
                            client.sendCommand({
                                "Id": cartridgeId
                            }, {
                                "Event": "MODIFY_SESSION",
                                "Session": updated_session
                            }).then(function (response) {
                                if(response.success) {
                                    console.log('SESSION UPDATED SUCCESS');
                                } else {
                                    console.log('SESSION UPDATED FAILED');
                                }
                            });

                            //get notes for selected slide 
                            var selected_slide_data = slides_inventory[slideindex];
                            var regex = /0,(.+),0/gm,
                                m = regex.exec(selected_slide_data.SpeakerNotes);
                            if(m) {
                                $('#speakerNotes').html('<p>' + m[1] + '</p>');
                            } else {
                                $('#speakerNotes').html('<p>' + selected_slide_data.SpeakerNotes + '</p>');
                            }

                            //update indicators
                            var target_slide_card = _.find($('.slide-ct .card'), function (itm, idx) {
                                return parseInt($(itm).data('positionIndex')) === slideindex;
                            });
                            if(updated.SpeakerNotes.length > 0) {
                                $(target_slide_card).find('.notes-icon').removeClass('d-none');
                            } else {
                                $(target_slide_card).find('.notes-icon').addClass('d-none');
                            }
                            if(updated.AVCommands.length > 0) {
                                $(target_slide_card).find('.light-icon').removeClass('d-none');
                            } else {
                                $(target_slide_card).find('.light-icon').addClass('d-none');
                            }

                            //hide modal
                            $('#detailsEditorModal').modal('hide');
                        });
                    });
                }

                //design page renderer
                function renderDesignPage (context) {
                    //slides
                    var slides = _.map(pgstate.get('current_area').Slides, function (itm) {
                        return itm;
                    });

                    var current_area_layout = _.findWhere(pgstate.get('valid_selected_areas'), {
                        Id: pgstate.get('current_area').Id
                    });

                    //determine layout style based on current area screens
                    var area_screen_count = current_area_layout ? current_area_layout.Children.length : 0;

                    //render call
                    context.partial('templates/design.template', {
                        session: pgstate.get('current_session'),
                        area: pgstate.get('current_area'),
                        areas_rest: pgstate.get('areas_rest'),
                        area_screen_count: area_screen_count,
                        is_leader: pgstate.get('is_leader'),
                        slides: slides
                    }, function () {
                        pgInitDesign(context);
                    });
                    
                }
                
                //GET: #/design/:sessionid
                this.get('#/design/:sessionid', function (context) {
                    renderDesignPage(context);
                });

                //PUSH TO DISPLAY
                function pushToAreaDisplay(data, displayed, reverse, current_lead_slide) {
                    client.sendCommand({
                        "Id": cartridgeId
                    }, {
                        "Event": "LOAD_ASSET",
                        "Data": data
                    }).then(function (response) {
                        if(response.success) {

                            var new_index = 0;
                            if(current_lead_slide) {

                                new_index = current_lead_slide.displayedId;
                                
                                //write new index in viewed area
                                var updated_area = _.extend({}, pgstate.get('current_area'), {
                                    CurrentIndex: new_index
                                });
                                pgstate.set('current_area', updated_area);
                                
                                //update area in areas
                                var updated_areas = _.map(pgstate.get('valid_selected_areas'), function (itm, idx) {
                                    if(itm.Id === updated_area.Id) {
                                        return updated_area;
                                    } else {
                                        return itm;
                                    }
                                });
                                pgstate.set('valid_selected_areas', updated_areas);

                                //update session with new areas
                                var updated_session = _.extend({}, pgstate.get('current_session'), {
                                    Areas: updated_areas
                                });
                                pgstate.set('current_session', updated_session);

                                //send update of session
                                client.sendCommand({
                                    "Id": cartridgeId
                                }, {
                                    "Event": "MODIFY_SESSION",
                                    "Session": updated_session
                                }).then(function (response) {
                                    if(response.success) {
                                        console.log('AREA CURRENTINDEX UPDATED SUCCESS');
                                    } else {
                                        console.log('AREA CURRENTINDEX UPDATED FAILED');
                                    }
                                });
                            }

                            $('.slide-ct .card').each(function (i, itm) {
                                $(itm).removeClass('is-selected');
                                $(itm).removeClass('is-displaying');
                                if(_.contains(displayed, i)) {
                                    $(itm).addClass('is-displaying');
                                    $(itm).addClass('is-selected');
                                };
                            });
                            //scroll into view
                            var last_displayed = ! reverse ? _.last($('.is-displaying')) : _.first($('.is-displaying'));
                            if(last_displayed) {
                                last_displayed.scrollIntoView({
                                    behavior: 'smooth',
                                    block: 'nearest'
                                });

                            }
                        }
                    });
                }

                //get available areas from layout
                function getRecursiveAreasFromLayout (layout) {
                    return _.reduce(layout, function (acc, itm, idx) {
                        if ((itm.Children && (itm.Children.length > 0)) && (itm.Type !== 'Area')) {
                            acc.push(getRecursiveAreasFromLayout(itm.Children));
                        } else {
                            acc.push(itm);
                        }
                        return acc;
                    }, []);
                }

                function listAllAreasFromLayout (layout) {
                    return _.flatten(getRecursiveAreasFromLayout(layout));
                }

                function showtimePushSelected() {

                    var current_area_layout = _.findWhere(pgstate.get('valid_selected_areas'), {
                        Id: pgstate.get('current_area').Id
                    });
                
                    var selected_slides = _.map($('.is-selected h4'), function (itm, idx) {
                        return _.extend({
                            displayedId: $(itm).closest('li.card').index()
                        }, pgstate.get('current_area').Slides[$(itm).closest('li.card').index()]);
                    });
                    
                    var data = _.map(current_area_layout.Children, function (itm, idx) {
                        return {
                            Screen: _.findWhere(pgstate.get('all_screens'), {
                                Id: itm.Id
                            }),
                            Asset: selected_slides[idx]
                        };
                    });

                    var displayed = _.map(selected_slides, function (itm, idx) {
                        return itm.displayedId;
                    });
                    pushToAreaDisplay(data, displayed, false, _.first(selected_slides));
                    return displayed;
                }

                //SHOWTIME VIEW
                function pgInitShowtime (context, push_current) {
                    var displayed = [],
                        layout_class = 'cards-x-';

                    var current_area_layout = _.findWhere(pgstate.get('valid_selected_areas'), {
                        Id: pgstate.get('current_area').Id
                    });

                    //determine layout style based on current area screens
                    var area_screen_count = current_area_layout ? current_area_layout.Children.length : 5;
                    layout_class += area_screen_count;

                    //init grid
                    $('#sortable').sortable({
                        forcePlaceholderSize: true,
                        forceHelperSize: true,
                        update: function (evt, ui) {
                            var list = _.map($('#sortable li h4.slide-number'), function (itm) {
                                return $(itm).data('slideIndex');
                            });
                            app.trigger('list.sorted', {
                                list: list
                            });
                        }
                    });
                    $('#sortable').disableSelection();
                    $('#sortable').sortable('disable');

                    setSliderCtHeight();

                    $('.slide-ct').mCustomScrollbar({
                        theme: '3d-thick-dark',
                        scrollInertia: 25,
                        autoHideScrollbar: false,
                        scrollButtons: {
                            enable: true
                        },
                        callbacks: {
                            onScrollStart: function () {
                                pgstate.set('showtime_isScrolling', true);
                            },
                            onScroll: function () {
                                pgstate.set('showtime_isScrolling', false);
                            }
                        }
                    });

                    //listen to leader changes
                    initLeaderToggle();

                    //listen to notes toggle
                    initNotesToggle();

                    //display style state
                    if(pgstate.get('current_area').Slides.length > 0) {
                        displayed = _.filter(_.map(current_area_layout.Children, function (itm, idx) {
                                var offset = pgstate.get('current_area').CurrentIndex || 0;
                                offset = Math.max(offset, 0);
                                return pgstate.get('current_area').Slides[idx + offset] ? idx + offset : -1;
                            }), function (itm, idx) {
                                return itm >= 0; //filter all negative indices
                            }); 
                        pgstate.set('showtime_displayed', displayed);
                    }

                    //init displayed state of slide (selected, displayed)
                    var init_compile = [];
                    $('.slide-ct .card').each(function (i, itm) {
                        //reset all display, selected flags
                        $(itm).removeClass('is-selected');
                        $(itm).removeClass('is-displaying');
                        
                        //set initial state
                        if(_.contains(pgstate.get('showtime_displayed'), i)) {
                            if(pgstate.get('current_area').CurrentIndex >= 0) {
                                //area is displaying if session area current idx is not negative
                                $(itm).addClass('is-displaying');
                            }
                            //is selected if listed in displayed
                            $(itm).addClass('is-selected');
                            init_compile.push(parseInt($(itm).find('h4').first().data('positionIndex')));
                        };

                        //click behavior (select group)
                        $(itm).click(function (evt) {
                            var listindex = $(evt.target).closest('li.card').index();

                            //find lead card of group clicked card is in
                            var leadslideindex = Math.floor(listindex / area_screen_count) * area_screen_count;
                            
                            //find all cards of selected group
                            var selected = _.filter($('.slide-ct .card h4'), function (itm, idx) {
                                return idx >= leadslideindex && idx < (leadslideindex + area_screen_count);
                            });

                            if(pgstate.get('showtime_isScrolling')) {
                                return;
                            }

                            //reset all cards selected indicator
                            $('.slide-ct .card').each(function (i, itm) {
                                $(itm).removeClass('is-selected');
                            });

                            //set selected row selected indicators
                            var to_compile = [];
                            _.each(selected, function (itm, idx, list) {
                                $(itm).closest('.slide-ct .card').addClass('is-selected');
                                to_compile.push($(itm).closest('.slide-ct .card').index());
                            });

                            //compile notes of selected slides
                            var selected_slides = _.filter(pgstate.get('current_area').Slides, function (itm, idx) {
                                return _.contains(to_compile, idx);
                            });

                            var compiled_notes = _.reduce(selected_slides, function (acc, itm) {
                                var regex = /0,(.+),0/gm,
                                    m = regex.exec(itm.SpeakerNotes);
                                if(m) {
                                    return acc + '<p>' + m[1] + '</p>';
                                } else {
                                    return acc + '<p>' + itm.SpeakerNotes + '</p>';
                                }
                            }, '');

                            //set speakernotes display with compiled notes
                            $('#speakerNotes').html(compiled_notes.length > 0 ? compiled_notes : '');

                            //block push if not leader
                            if(!pgstate.get('is_leader')) {
                                evt.preventDefault();
                                return;
                            } else {
                                pgstate.set('showtime_displayed', showtimePushSelected());               
                            }
                        });

                    });

                    //init notes on current initial selection
                    var init_selected_slides = _.filter(pgstate.get('current_area').Slides, function (itm, idx) {
                        return _.contains(init_compile, idx);
                    });
                    var init_compiled_notes = _.reduce(init_selected_slides, function (acc, itm, idx) {
                        var regex = /0,(.+),0/gm,
                            m = regex.exec(itm.SpeakerNotes);
                        if(m) {
                            return acc + '<p>' + m[1] + '</p>'
                        } else {
                            return acc + '<p>' + itm.SpeakerNotes + '</p>';
                        }
                    }, '');
                    $('#speakerNotes').html(init_compiled_notes.length > 0 ? init_compiled_notes : '');

                    $('#btnPush').click(function () {
                        pgstate.set('showtime_displayed', showtimePushSelected());                                
                    });

                    //Advance NEXT button
                    var  end_reached = false, start_reached = false;
                    $('#btnNext').click(function () {
                        var first = _.first(pgstate.get('showtime_displayed'));
                        var last = _.last(pgstate.get('showtime_displayed'));
                        var leadslideindex = first;
                        start_reached = false;

                        if(end_reached) {
                            var next_area = getNextArea(pgstate.get('current_area'));
                            end_reached = false;
                            if(next_area) {
                                pgstate.set('current_area', _.extend(_.findWhere(pgstate.get('valid_selected_areas'), {
                                    Id: next_area.Id
                                }), {
                                    CurrentIndex: 0
                                }));
                                pgstate.set('areas_rest', _.filter(pgstate.get('valid_selected_areas'), function (itm, idx) {
                                    return itm.Id !== next_area.Id;
                                }));

                                client.sendCommand({
                                    "Id": cartridgeId
                                }, {
                                    "Event": "SWITCH_AREA",
                                    "Area": {
                                        Id: next_area.Id,
                                        CurrentIndex: 0,
                                        SessionId: pgstate.get('current_session').Id
                                    }
                                }).then(function (response) {
                                    if(response.success) {
                                        console.log('PRESENTER AREA SWAPPED SUCCESS');
                                    } else {
                                        console.log('PRESENTER AREA SWAPPED FAILED');
                                    }
                                });

                                renderShowtimePage(context, true);    
                            }
                        } else {

                            if(first + area_screen_count < pgstate.get('current_area').Slides.length) {
                                leadslideindex = Math.max(0, (Math.floor(last / area_screen_count) + 1) * area_screen_count);
                            } else {
                                end_reached = true;
                            }
    
                            var selected_slides = _.map(_.filter($('.slide-ct h4'), function (itm, idx) {
                                    return idx >= leadslideindex && idx < (leadslideindex + area_screen_count);
                                }), function (itm, idx) {
                                    return _.extend({
                                        displayedId: $(itm).closest('li.card').index()
                                    }, pgstate.get('current_area').Slides[$(itm).closest('li.card').index()]);
                                });
    
                            var data = _.map(current_area_layout.Children, function (itm, idx) {
                                return {
                                    Screen: _.findWhere(pgstate.get('all_screens'), {
                                        Id: itm.Id
                                    }),
                                    Asset: selected_slides[idx]
                                };
                            });
        
                            pgstate.set('showtime_displayed', _.map(selected_slides, function (itm, idx) {
                                return itm.displayedId;
                            }));
    
                            //compile notes of selected slides
                            var compiled_notes = _.reduce(selected_slides, function (acc, itm) {
                                var regex = /0,(.+),0/gm,
                                    m = regex.exec(itm.SpeakerNotes);
                                if(m) {
                                    return acc + '<p>' + m[1] + '</p>';
                                } else {
                                    return acc + '<p>' + itm.SpeakerNotes + '</p>';
                                }
                            }, '');
    
                            //set speakernotes display with compiled notes
                            $('#speakerNotes').html(compiled_notes.length > 0 ? compiled_notes : '');
                            pushToAreaDisplay(data, pgstate.get('showtime_displayed'), false, _.first(selected_slides));
                        }
                    });

                    //Advance PREV button
                    $('#btnPrev').click(function () {
                        //leadindex calculate
                        var first = _.first(pgstate.get('showtime_displayed'));
                        var last = _.last(pgstate.get('showtime_displayed'));
                        var leadslideindex = first;
                        end_reached = false;

                        if (start_reached) {
                            var prev_area = getPrevArea(pgstate.get('current_area'));
                            start_reached = false;
                            if(prev_area) {
                                pgstate.set('current_area', _.extend(_.findWhere(pgstate.get('valid_selected_areas'), {
                                    Id: prev_area.Id
                                }), {
                                    CurrentIndex: Math.floor(prev_area.Slides.length / prev_area.Children.length) * prev_area.Children.length - 1
                                }));
                                pgstate.set('areas_rest', _.filter(pgstate.get('valid_selected_areas'), function (itm, idx) {
                                    return itm.Id !== prev_area.Id;
                                }));

                                client.sendCommand({
                                    "Id": cartridgeId
                                }, {
                                    "Event": "SWITCH_AREA",
                                    "Area": {
                                        Id: prev_area.Id,
                                        CurrentIndex: prev_area.Slides.length - prev_area.Children.length,
                                        SessionId: pgstate.get('current_session').Id
                                    }
                                }).then(function (response) {
                                    if(response.success) {
                                        console.log('PRESENTER AREA SWAPPED SUCCESS');
                                    } else {
                                        console.log('PRESENTER AREA SWAPPED FAILED');
                                    }
                                });

                                renderShowtimePage(context, true);    
                            }
                        } else {

                            if(last - area_screen_count < 0) {
                                start_reached = true;
                            } else {
                                leadslideindex = Math.max(0, (Math.floor(last / area_screen_count) - 1) * area_screen_count);
                            }
    
                            //get selected content
                            var selected_slides = _.map(_.filter($('.slide-ct h4'), function (itm, idx) {
                                    return idx >= leadslideindex && idx < (leadslideindex + area_screen_count);
                                }), function (itm, idx) {
                                    return _.extend({
                                        displayedId: $(itm).closest('li.card').index()
                                    }, pgstate.get('current_area').Slides[$(itm).closest('li.card').index()]);
                                });
    
                            var data = _.map(current_area_layout.Children, function (itm, idx) {
                                return {
                                    Screen: _.findWhere(pgstate.get('all_screens'), {
                                        Id: itm.Id
                                    }),
                                    Asset: selected_slides[idx]
                                };
                            });
    
                            pgstate.set('showtime_displayed', _.map(selected_slides, function (itm, idx) {
                                return itm.displayedId;
                            }));
    
                            //compile notes of selected slides
                            var compiled_notes = _.reduce(selected_slides, function (acc, itm) {
                                var regex = /0,(.+),0/gm,
                                    m = regex.exec(itm.SpeakerNotes);
                                if(m) {
                                    return acc + '<p>' + m[1] + '</p>';
                                } else {
                                    return acc + '<p>' + itm.SpeakerNotes + '</p>';
                                }
                            }, '');
    
                            //set speakernotes display with compiled notes
                            $('#speakerNotes').html(compiled_notes.length > 0 ? compiled_notes : '');
                            pushToAreaDisplay(data, pgstate.get('showtime_displayed'), true, _.first(selected_slides));
                        }

                    });

                    //Listen to select area update
                    $('#areaselect .dropdown-menu button').click(function (evt) {
                        var areaid = $(evt.target).data('areaId');
                        // start_reached = false;
                        // end_reached  = false;
                        pgstate.set('current_area', _.findWhere(pgstate.get('valid_selected_areas'), {
                            Id: areaid
                        }))
                        pgstate.set('areas_rest', _.filter(pgstate.get('valid_selected_areas'), function (itm, idx) {
                            return itm.Id !== areaid;
                        }))
                        renderShowtimePage(context);
                    });

                    if(push_current) {
                        pgstate.set('showtime_displayed', showtimePushSelected());
                    }

                    //Listen to grid layout select update
                    initGridLayoutToggle(layout_class);
                    initVolumeBar();
                    initStopwatch();
                }

                function renderShowtimePage (context, push_current) {
                    //slides
                    var slides = _.map(pgstate.get('current_area').Slides, function (itm) {
                        return itm;
                    });
                    //layout
                    var current_area_layout = _.findWhere(pgstate.get('valid_selected_areas'), {
                        Id: pgstate.get('current_area').Id
                    });
                    //determine layout style based on current area screens
                    var area_screen_count = current_area_layout ? current_area_layout.Children.length : 0;

                    context.partial('templates/showtime.template', {
                        session: pgstate.get('current_session'),
                        area: pgstate.get('current_area'),
                        area_screen_count: area_screen_count, 
                        areas_rest: pgstate.get('areas_rest'),
                        is_leader: pgstate.get('is_leader'),
                        slides: slides,
                        audio: pgstate.get('audio') || {}
                    }, function () {
                        pgInitShowtime(context, push_current);
                        _.delay(scrollToDisplayed, 200);
                    });
                }

                //GET: #/showtime/:sessionid
                this.get('#/showtime/:sessionid', function (context) {
                    var params = this.params;
                    if(!pgstate.exists('current_session')) {
                        client.sendCommand({
                            "Id": cartridgeId
                        }, {
                            "Event": "GET_LAYOUT",
                            "Data": {
                                "Type": "Administrator",
                                "NetworkId": getNetworkID()
                            }
                        }).then(function (response) {
                            if(response.success) {
                                //available areas
                                pgstate.set('current_network_layout', response.data);
                                var available_areas = _.map(listAllAreasFromLayout(pgstate.get('current_network_layout')), function (itm, idx) {
                                    return _.extend({
                                        CurrentIndex: 0,
                                        Id: "",
                                        Name: "",
                                        NetworkId: getNetworkID(),
                                        Screens: [],
                                        Slides: [],
                                        State: ""
                                    }, itm);
                                });
                                pgstate.set('available_areas', available_areas);

                                //get all areas
                                client.sendCommand({
                                    "Id": cartridgeId
                                }, {
                                    "Event": "GET_ALL_SCREENS"
                                }).then(function (response) {

                                    if(response.success) {
                                        pgstate.set('all_screens', response.data);

                                        client.sendCommand({
                                            "Id": cartridgeId
                                        }, {
                                            "Event": "JOIN_SESSION",
                                            "Session": {
                                                "Id": params['sessionid']
                                            }
                                        }).then(function (response) {
                                            var valid_selected_areas;
                                            if(response.success) {
                                                //data - current session
                                                pgstate.set('current_session', response.data.Session);
                                                pgcookie.set('current_session', {
                                                    Id: response.data.Session.Id
                                                });
                                                //valid selected areas
                                                valid_selected_areas = orderAreaList(_.map(_.filter(response.data.Session.Areas, function (itm, idx) {
                                                    return isValidArea(itm);
                                                }), function (itm, idx) {
                                                    return injectAreaDetails(itm);
                                                }));
                                                pgstate.set('valid_selected_areas', valid_selected_areas);
                                                //current area
                                                pgstate.set('current_area', _.first(pgstate.get('valid_selected_areas')) || _.first(pgstate.get('available_areas')));
                                                pgstate.set('areas_rest', _.filter(valid_selected_areas, function (itm) {
                                                    return itm.Id !== pgstate.get('current_area').Id;
                                                }));
                                                //is leader
                                                pgstate.set('is_leader', response.data.IsLeader);
                                                renderShowtimePage(context);
                                            } else {
                                                context.partial('templates/error.template', {
                                                    item: {
                                                        message: 'Join Session Failed.'
                                                    }
                                                });        
                                            }
                                        });
        
                                    } else {
                                        context.partial('templates/error.template', {
                                            item: {
                                                message: 'Get ALL Screens Failed.'
                                            }
                                        });
                                    }
                                });

                            } else {
                                context.partial('templates/error.template', {
                                    item: {
                                        message: 'Get Layout Failed.'
                                    }
                                });
                            }
                        });
    
                    } else {
                        renderShowtimePage(context);
                    }
                });

                function initOffscriptAreaSelector(area_selected, area_rest, sessionid) {
                    var current_offset_area_layout = _.findWhere(pgstate.get('available_areas'), {
                        Id: area_selected.Id
                    });

                    //setup dropdown button
                    $('#offscript-session-area-selected').text(current_offset_area_layout.Name);
                    $('#offscript-session-area-selected').data('areaId', area_selected.Id)
                    //setup dropdown menu
                    var area_options_html = _.reduce(area_rest, function (acc, itm, idx) {
                        var current_offset_area_layout = _.findWhere(pgstate.get('available_areas'), {
                            Id: itm.Id
                        });    
                        var out = 
                            '<button class="dropdown-item frutiger-roman font-26" data-area-id="'
                            + itm.Id + '">' + current_offset_area_layout.Name + '</button>';
                        return acc + out;
                    }, '');
                    $('#offscript-session-area-list').html(area_options_html);
                    //init menu button clicks
                    _.defer(function () {
                        $('#offscript-session-area-list button').click(function (evt) {
                            var selected_offscript_session = _.findWhere(pgstate.get('offscript_sessions'), {
                                Id: sessionid
                            });
                            var area_selected_id = $(evt.currentTarget).data('areaId');
                            var area_selected = _.findWhere(selected_offscript_session.Areas, {
                                Id: area_selected_id
                            });
                            var area_rest = _.filter(selected_offscript_session.Areas, function (itm, idx) {
                                return itm.Id !== area_selected_id;
                            });
                            initOffscriptAreaSelector(area_selected, area_rest, sessionid);
                        });
                    });
                }

                //OFFSCRIPT SELECT
                function pgInitOffscriptSelect (context) {
                    pgstate.set('offscript_select_isScrolling', false);

                    var main_height = $('#main').outerHeight(true)
                        subnav_height = $('#pg-subnav-ct').outerHeight(true),
                        modetabs_height = $('#pg-mode-tabs').outerHeight(true),
                        instructions_height = $('#pg-instruction-message').outerHeight(true);

                    $('.sessions-list-ct').outerHeight(main_height - subnav_height - 
                            modetabs_height - instructions_height - 15);

                    $('.sessions-list-ct').mCustomScrollbar({
                        theme: '3d-thick-dark',
                        snapAmount: 60,
                        scrollInertia: 25,
                        autoHideScrollbar: false,
                        scrollButtons: {
                            enable: true
                        },
                        callbacks: {
                            onScrollStart: function () {
                                pgstate.set('offscript_select_isScrolling', true);
                            },
                            onScroll: function () {
                                pgstate.set('offscript_select_isScrolling', false);
                            }
                        }
                    });

                    $('.off-session-item button').click(function (evt) {
                        var sessionid = $(evt.currentTarget).data('sessionId');
                        var offscript_session_selected = _.findWhere(pgstate.get('offscript_sessions'), {
                            Id: sessionid
                        });
                        var session_selected_areas = offscript_session_selected.Areas;
                        var area_first = _.first(session_selected_areas);
                        var area_rest = _.rest(session_selected_areas);

                        if(pgstate.get('offscript_select_isScrolling')) {
                            return;
                        }

                        initOffscriptAreaSelector(area_first, area_rest, sessionid);

                        $('#offscriptModalLabel').html($(evt.currentTarget).text());
                        $('#select-offscript').unbind('click').click(function (evt) {
                            var selected_area_id = $('#offscript-session-area-selected').data('areaId');
                            app.runRoute('post', '#/offscript/select/area', {
                                sessionid: sessionid,
                                areaid: selected_area_id
                            });
                            $('#offscriptModal').modal('hide');
                        });
                        $('#offscriptModal').modal('show');
                    });
                }

                this.get('#/offscript/select', function (context) {
                    client.sendCommand({
                        "Id": cartridgeId
                    }, {
                        "Event": "GET_ACTIVE_SESSIONS"
                    }).then(function (response) {
                        if(response.success) {
                            pgstate.set('offscript_sessions', response.data);
                            context.partial('templates/offscriptselection.template', {
                                items: pgstate.get('offscript_sessions'),
                                session: pgstate.get('current_session'),
                                area: pgstate.get('current_area'),
                            }, function () {
                                pgInitOffscriptSelect(context);
                            });
                        }
                    });
                });

                function offscriptPushSelected () {

                    var current_area_layout = _.findWhere(pgstate.get('valid_selected_areas'), {
                        Id: pgstate.get('current_area').Id
                    });

                    var selected_slides = _.map($('.is-selected h4'), function (itm, idx) {
                        return _.extend({
                            displayedId: parseInt($(itm).closest('li.card').index())
                        }, pgstate.get('current_offscript_area').Slides[parseInt($(itm).data('positionIndex'))]);
                    });

                    var data = _.map(current_area_layout.Children, function (itm, idx) {
                        return {
                            Screen: _.findWhere(pgstate.get('all_screens'), {
                                Id: itm.Id
                            }),
                            Asset: selected_slides[idx]
                        };
                    });

                    var displayed = _.map(selected_slides, function (itm, idx) {
                        return itm.displayedId;
                    });

                    pushToAreaDisplay(data, displayed);
                    return displayed;
                }

                //OFFSCRIPT AREA SELECT
                function pgInitOffscript (context) {
                    var displayed = [],
                        layout_class = 'cards-x-';
                    var current_area_layout = _.findWhere(pgstate.get('valid_selected_areas'), {
                        Id: pgstate.get('current_area').Id
                    });
    
                    //determine layout style based on current area screens
                    var area_screen_count = current_area_layout ? current_area_layout.Children.length : 5;
                    layout_class += area_screen_count;

                    //init grid
                    $('#sortable').sortable({
                        forcePlaceholderSize: true,
                        forceHelperSize: true,
                        update: function (evt, ui) {
                            context.log('ORDER CHANGED - UPDATED', evt, ui);
                            var list = _.map($('#sortable li h4.slide-number'), function (itm) {
                                return $(itm).data('positionIndex');
                            });
                            app.trigger('list.sorted', {
                                list: list
                            });
                        }
                    });
                    $('#sortable').disableSelection();
                    $('#sortable').sortable('disable');
                    pgstate.set('offscript_isScrolling', false);

                    setSliderCtHeight();

                    $('.slide-ct').mCustomScrollbar({
                        theme: '3d-thick-dark',
                        scrollInertia: 25,
                        autoHideScrollbar: false,
                        scrollButtons: {
                            enable: true
                        },
                        callbacks: {
                            onScrollStart: function () {
                                pgstate.set('offscript_isScrolling', true);
                            },
                            onScroll: function () {
                                pgstate.set('offscript_isScrolling', false);
                            }
                        }
                    });

                    //listen to leader changes
                    initLeaderToggle();

                    //listen to Editable changes
                    initEditToggle();

                    var init_compile = [];
                    $('.slide-ct .card').each(function (i, itm) {
                        var that = this;
                        $(itm).removeClass('is-selected');
                        $(itm).removeClass('is-displaying');

                        //card click
                        $(itm).click(function (evt) {

                            var listindex = $(evt.target).closest('.slide-ct .card').index();
                            //find lead card of group clicked card is in
                            var leadslideindex = Math.floor(listindex / area_screen_count) * area_screen_count;
                            //find all cards of selected group
                            var selected = _.filter($('.slide-ct .card h4'), function (itm, idx) {
                                return idx >= leadslideindex && idx < (leadslideindex + area_screen_count);
                            });

                            if(pgstate.get('offscript_isScrolling')) {
                                return;
                            }

                            //reset all cards selected indicator
                            $('.slide-ct .card').each(function (i, itm) {
                                $(itm).removeClass('is-selected');
                            });

                            //set selected row selected indicators
                            var to_compile = [];
                            _.each(selected, function (itm, idx, list) {
                                $(itm).closest('.slide-ct .card').addClass('is-selected');
                                to_compile.push(parseInt($(itm).data('positionIndex')));
                            })

                            var selected_slides = _.filter(pgstate.get('current_offscript_area').Slides, function (itm, idx) {
                                return _.contains(to_compile, idx);
                            });

                            var compiled_notes = _.reduce(selected_slides, function (acc, itm) {
                                var regex = /0,(.+),0/gm,
                                    m = regex.exec(itm.SpeakerNotes);
                                if(m) {
                                    return acc + '<p>' + m[1] + '</p>';
                                } else {
                                    return acc + '<p>' + itm.SpeakerNotes + '</p>';
                                }
                                
                            }, '');

                            $('#speakerNotes').html(compiled_notes.length > 0 ? compiled_notes : '');

                            var editablestate = $('#iseditable').parent().find('input:checked').length > 0;
                            if(!editablestate) {
                                displayed = offscriptPushSelected();
                            }
                        });
                    });

                    //listen to notes toggle
                    initNotesToggle();

                    //btn PUSH
                    $('#btnPush').click(function () {
                        displayed = offscriptPushSelected();
                    });

                    //Advance NEXT button
                    $('#btnNext').click(function () {
                        var first = _.first(displayed);
                        var last = _.last(displayed);
                        var leadslideindex = first;
                        if(first + area_screen_count < pgstate.get('current_offscript_area').Slides.length) {
                            leadslideindex = Math.max(0, (Math.floor(last / area_screen_count) + 1) * area_screen_count);
                        }

                        var selected_slides = _.map(_.filter($('.slide-ct h4'), function (itm, idx) {
                            return idx >= leadslideindex && idx < (leadslideindex + area_screen_count);
                        }), function (itm, idx) {
                            return _.extend({
                                displayedId: $(itm).closest('li.card').index()
                            }, pgstate.get('current_offscript_area').Slides[parseInt($(itm).data('positionIndex'))]);
                        });

                        var data = _.map(current_area_layout.Children, function (itm, idx) {
                            return {
                                Screen: _.findWhere(pgstate.get('all_screens'), {
                                    Id: itm.Id
                                }),
                                Asset: selected_slides[idx]
                            };
                        });

                        displayed = _.map(selected_slides, function (itm, idx) {
                            return itm.displayedId;
                        });

                        //compile notes of selected slides
                        var compiled_notes = _.reduce(selected_slides, function (acc, itm) {
                            var regex = /0,(.+),0/gm,
                                m = regex.exec(itm.SpeakerNotes);
                            if(m) {
                                return acc + '<p>' + m[1] + '</p>';
                            } else {
                                return acc + '<p>' + itm.SpeakerNotes + '</p>';
                            }
                        }, '');

                        //set speakernotes display with compiled notes
                        $('#speakerNotes').html(compiled_notes.length > 0 ? compiled_notes : '');
                        pushToAreaDisplay(data, displayed);
                    });

                    //Advance PREV button
                    $('#btnPrev').click(function () {
                        var first = _.first(displayed);
                        var last = _.last(displayed);
                        var leadslideindex = first;
                        leadslideindex = Math.max(0, (Math.floor(last / area_screen_count) - 1) * area_screen_count);

                        //get selected content
                        var selected_slides = _.map(_.filter($('.slide-ct h4'), function (itm, idx) {
                            return idx >= leadslideindex && idx < (leadslideindex + area_screen_count);
                        }), function (itm, idx) {
                            return _.extend({
                                displayedId: $(itm).closest('li.card').index()
                            }, pgstate.get('current_offscript_area').Slides[$(itm).closest('li.card').index()]);
                        });

                        var data = _.map(current_area_layout.Children, function (itm, idx) {
                            return {
                                Screen: _.findWhere(pgstate.get('all_screens'), {
                                    Id: itm.Id
                                }),
                                Asset: selected_slides[idx]
                            };
                        });

                        displayed = _.map(selected_slides, function (itm, idx) {
                            return itm.displayedId;
                        });

                        //compile notes of selected slides
                        var compiled_notes = _.reduce(selected_slides, function (acc, itm) {
                            var regex = /0,(.+),0/gm,
                                m = regex.exec(itm.SpeakerNotes);
                            if(m) {
                                return acc + '<p>' + m[1] + '</p>';
                            } else {
                                return acc + '<p>' + itm.SpeakerNotes + '</p>';
                            }
                        }, '');

                        //set speakernotes display with compiled notes
                        $('#speakerNotes').html(compiled_notes.length > 0 ? compiled_notes : '');
                        pushToAreaDisplay(data, displayed, true);
                    });

                    //Listen to grid layout select update
                    initGridLayoutToggle(layout_class);
                    initVolumeBar();
                    initStopwatch();
                }

                this.post('#/offscript/select/area', function (context) {
                    var slides = [];
                    var layout_class = 'cards-x-';
                    var current_area_layout = _.findWhere(pgstate.get('valid_selected_areas'), {
                        Id: pgstate.get('current_area').Id
                    });
    
                    //determine layout style based on current area screens
                    var area_screen_count = current_area_layout ? current_area_layout.Children.length : 5;
                    layout_class += area_screen_count;

                    //set session
                    pgstate.set('current_offscript_session', _.findWhere(pgstate.get('offscript_sessions'), {
                        Id: this.params.sessionid
                    }));
                    pgstate.set('current_offscript_area', _.findWhere(pgstate.get('current_offscript_session').Areas, {
                        Id: this.params.areaid
                    }));
                    slides = pgstate.get('current_offscript_area').Slides;
                    
                    context.partial('templates/offscript.template', {
                        session: pgstate.get('current_session'),
                        area: pgstate.get('current_area'),
                        area_screen_count: area_screen_count,
                        offscript: {
                            session: pgstate.get('current_offscript_session'),
                            area: pgstate.get('current_offscript_area')
                        },
                        slides: slides,
                        audio: pgstate.get('audio') || {}
                    }, function () {
                        pgInitOffscript(context);
                        _.delay(scrollToDisplayed, 200);
                    });
                });

                //EVENT log.in
                this.bind('log.in', function () {
                    this.redirect('#/sessions');
                });

                //EVENT log.out
                this.bind('log.out', function () {
                    client.sendCommand({
                        "Id": cartridgeId
                    },{
                        "Event": "UNREGISTER"
                    }).then(function (response) {
                        console.log(response);
                    });

                    $('#logoutModal').modal('hide');
                    pgstate.clearAll();
                    pgcookie.clearAll();

                    //LOGOUT X2O
                    var logouturl = window.location.origin + '/XManagerWeb/REST/v1/logout';
                    $.ajax(logouturl, {
                        method: 'GET',
                        accept: 'application/json',
                        contentType: 'application/json'            
                    }).then(function (response) {
                        if(response.result.success) {
                            _.defer(function () {
                                window.top.location.reload();
                            });
                        }
                    });
                });

                //EVENT list.sorted
                this.bind('list.sorted', function (evt, data) {
                    console.log(data);
                })

                //EVENT x2o.epoxy.push
                this.bind('x2o.epoxy.push', function (evt, dat) {
                    switch(dat.Event) {
                        case 'SESSION_MODIFIED':
                        case 'SESSION_LOADED':
                            if(dat.Session.Id === pgstate.get('current_session').Id) {
                                pgstate.set('current_session', dat.Session);
                                var displayed = [];

                                //valid selected areas
                                valid_selected_areas = orderAreaList(_.map(_.filter(dat.Session.Areas, function (itm, idx) {
                                    return isValidArea(itm);
                                }), function (itm, idx) {
                                    return injectAreaDetails(itm);
                                }));
                                pgstate.set('valid_selected_areas', valid_selected_areas);

                                var current_area_layout = _.findWhere(pgstate.get('valid_selected_areas'), {
                                    Id: pgstate.get('current_area').Id
                                });

                                pgstate.set('current_area', _.extend(current_area_layout, _.findWhere(dat.Session.Areas, {
                                    Id: pgstate.get('current_area').Id
                                })));
            
                                //determine layout style based on current area screens
                                var area_screen_count = current_area_layout ? current_area_layout.Children.length : 5;

                                var slides = pgstate.get('current_area').Slides;
                                var updatehtml = _.reduce(slides, function (acc, itm, idx) {
                                    var out = 
                                        '<li class="card card-inverse ui-state-default ui-sortable-handle d-flex align-items-center">'
                                        + '<img class="card-img" src="' + itm.Thumbnail + '" alt="slide-' + itm.Asset + '__' + itm.Index + '">'
                                        + '<div class="card-img-overlay p-0">'
                                        + '<h4 class="slide-number frutiger-roman font-22" data-position-index="' + idx + '" data-slide-index="' + itm.Asset + '__' + itm.Index + '">' + (idx + 1) + '</h4>'
                                        + (itm.SpeakerNotes.length > 0 ? '<img src="./images/notes-icon@2x.png" class="notes-icon">' : '')
                                        + (itm.AVCommands.length > 0 ? '<img src="./images/light-changes-icon@2x.png" class="light-icon">' : '')
                                        + '<div class="selected-icon-ct"></div>'
                                        + '</div>'
                                        + '</li>';
                                    return acc + out;
                                }, '');

                                if(pgstate.get('current_area').Slides.length > 0) {
                                    displayed = _.filter(_.map(current_area_layout.Children, function (itm, idx) {
                                            var offset = pgstate.get('current_area').CurrentIndex || 0;
                                            offset = Math.max(offset, 0);
                                            return pgstate.get('current_area').Slides[idx + offset] ? idx + offset : -1;
                                        }), function (itm, idx) {
                                            return itm >= 0; //filter all negative indices
                                        }); 
                                    pgstate.set('showtime_displayed', displayed);
                                }
            
                                $('#sortable').html(updatehtml);
                                _.defer(function () {
                                    var init_compile = [];
                                    $('.slide-ct .card').each(function (i, itm) {
                                        var that = this;
                                        $(itm).removeClass('is-selected');
                                        $(itm).removeClass('is-displaying');

                                        //set initial state
                                        if(_.contains(pgstate.get('showtime_displayed'), i)) {
                                            if(pgstate.get('current_area').CurrentIndex >= 0) {
                                                //area is displaying if session area current idx is not negative
                                                $(itm).addClass('is-displaying');
                                            }
                                            $(itm).addClass('is-displaying');
                                            //is selected if listed in displayed
                                            $(itm).addClass('is-selected');
                                            init_compile.push(parseInt($(itm).find('h4').first().data('positionIndex')));
                                        };
                                        
                                        $(itm).click(function (evt) {

                                            var listindex = $(evt.target).closest('li.card').index();
                                            //find lead card of group clicked card is in
                                            var leadslideindex = Math.floor(listindex / area_screen_count) * area_screen_count;

                                            //find all cards of selected group
                                            var selected = _.filter($('.slide-ct .card h4'), function (itm, idx) {
                                                return idx >= leadslideindex && idx < (leadslideindex + area_screen_count);
                                            });

                                            //reset all cards selected indicator
                                            $('.slide-ct .card').each(function (i, itm) {
                                                $(itm).removeClass('is-selected');
                                            });

                                            //set selected row selected indicators
                                            var to_compile = [];
                                            _.each(selected, function (itm, idx, list) {

                                                $(itm).closest('.slide-ct .card').addClass('is-selected');
                                                to_compile.push($(itm).closest('.slide-ct .card').index());

                                            })
                
                                            var selected_slides = _.filter(pgstate.get('current_area').Slides, function (itm, idx) {
                                                return _.contains(to_compile, idx);
                                            });

                                            var compiled_notes = _.reduce(selected_slides, function (acc, itm) {
                                                var regex = /0,(.+),0/gm,
                                                    m = regex.exec(itm.SpeakerNotes);
                                                if(m) {
                                                    return acc + '<p>' + m[1] + '</p>';
                                                } else {
                                                    return acc + '<p>' + itm.SpeakerNotes + '</p>';
                                                }
                                                
                                            }, '');
                                            $('#speakerNotes').html(compiled_notes.length > 0 ? compiled_notes : '');
                                            pgstate.set('showtime_displayed', showtimePushSelected());               
                                        });
                                    });

                                    //init notes on current initial selection
                                    var init_selected_slides = _.filter(pgstate.get('current_area').Slides, function (itm, idx) {
                                        return _.contains(init_compile, idx);
                                    });
                                    var init_compiled_notes = _.reduce(init_selected_slides, function (acc, itm) {
                                        var regex = /0,(.+),0/gm,
                                            m = regex.exec(itm.SpeakerNotes);
                                        if(m) {
                                            return acc + '<p>' + m[1] + '</p>';
                                        } else {
                                            return acc + '<p>' + itm.SpeakerNotes + '</p>';
                                        }
                                        
                                    }, '');
                                    $('#speakerNotes').html(init_compiled_notes.length > 0 ? init_compiled_notes : '');
                                    
                                    var last_displayed = _.last($('.is-displaying'));
                                    if(last_displayed) {
                                        last_displayed.scrollIntoView({
                                            behavior: 'smooth'
                                        });                        
                                    }
                                });                 
                            }
                            break;
                        case 'NEW_LEADER':
                            pgstate.set('is_leader', true);
                            app.refresh();
                            break;
                        case 'LEADER_LOST':
                            pgstate.set('is_leader', false);
                            app.refresh();
                            break;
                        case 'AREA_SWITCHED':
                            if(!pgstate.get('is_leader')) {
                                if(dat.Area.SessionId === pgstate.get('current_session').Id) {
                                    pgstate.set('current_area', _.extend(_.findWhere(pgstate.get('valid_selected_areas'), {
                                        Id: dat.Area.Id
                                    }), {
                                        CurrentIndex: dat.Area.CurrentIndex
                                    }));
                                    pgstate.set('areas_rest', _.filter(pgstate.get('valid_selected_areas'), function (itm, idx) {
                                        return itm.Id !== dat.Area.Id;
                                    }));
                                    app.refresh();
                                }
                            }
                            break;
                    }
                });

                //EVENT leader.surrendered
                this.bind('leader.surrendered', function (evt, dat) {
                    client.sendCommand({
                        "Id": cartridgeId
                    },{
                        "Event": "RELEASE_LEADER"
                    }).then(function (response) {
                        console.log('LEADER.SURRENDERED');
                    });

                    pgstate.set('is_leader', false);
                    this.redirect(dat.destination);
                });

                //EVENT leader.requested
                this.bind('leader.obtain', function (evt, dat) {
                    client.sendCommand({
                        "Id": cartridgeId
                    },{
                        "Event": "TAKE_LEADER"
                    }).then(function (response) {
                        console.log('LEADER.OBTAINED');
                    });

                    pgstate.set('is_leader', true);
                    this.redirect(dat.destination);
                });

                //EVENT join session
                this.bind('join.session', function (evt, dat) {
                    this.redirect('#/showtime/' + dat.sessionid);
                })

            });
    
            $(function () {
                app.run('#/');
            });
    
        });
    });
})();